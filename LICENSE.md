# Godot Engine

This game was made with Godot Engine, which is available under the following license:

> Copyright © 2007-2021 Juan Linietsky, Ariel Manzur.
> Copyright © 2014-2021 Godot Engine contributors.
>
> Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
>
> The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
>
> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
>
> -- [Godot Engine](https://godotengine.org)

# The game itself

## Images

Copyright © 2021 [ent](https://codeberg.org/ent)  
Copyright © 2021 [papojari](https://codeberg.org/papojari)  
Copyright © 2021 [MaxBlxck](https://codeberg.org/MaxBlxck)  
Copyright © 2021 [juicy](https://codeberg.org/juicy)  
Copyright © 2021 [justus](https://codeberg.org/justus)  
Copyright © 2021 [NEXDT](https://codeberg.org/NEXDT)  
Copyright © 2021 [niklas_lodwig](https://codeberg.org/niklas_lodwig)  
Copyright © 2021 [Ole_Koenig_der_Dinonuggies](https://codeberg.org/Ole_Koenig_der_Dinonuggies)  
Copyright © 2021 [paulderpanzerfahrer](https://codeberg.org/paulderpanzerfahrer)  
Copyright © 2021 [Schnau](https://codeberg.org/Schnau)

All images of the game are licensed under [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/legalcode) and have the above copyright.

## Code

All of the code and files other than images are licensed under the following license:

---

**Dinonuggy's Journey**  
*A game where a fried nugget in from of a Dino has to escape a kitchen.*

Copyright © 2021 [ent](https://codeberg.org/ent)  
Copyright © 2021 [papojari](https://codeberg.org/papojari)  
Copyright © 2021 [MaxBlxck](https://codeberg.org/MaxBlxck)  
Copyright © 2021 [juicy](https://codeberg.org/juicy)  
Copyright © 2021 [justus](https://codeberg.org/justus)  
Copyright © 2021 [NEXDT](https://codeberg.org/NEXDT)  
Copyright © 2021 [niklas_lodwig](https://codeberg.org/niklas_lodwig)  
Copyright © 2021 [Ole_Koenig_der_Dinonuggies](https://codeberg.org/Ole_Koenig_der_Dinonuggies)  
Copyright © 2021 [paulderpanzerfahrer](https://codeberg.org/paulderpanzerfahrer)  
Copyright © 2021 [Schnau](https://codeberg.org/Schnau)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see [https://www.gnu.org/licenses/](https://www.gnu.org/licenses/).
